import { RegisterComponent } from './forms/register/register.component';
import { SportsComponent } from './../../../luispAngular/src/app/sports/sports.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//import { pathToFileURL } from 'url';

const routes: Routes = [
  {path: 'deportes', component : SportsComponent},
  {path: 'register', component : RegisterComponent},
  {path: '', redirectTo : 'Sports', pathMatch:'full'},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

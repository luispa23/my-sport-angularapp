
import { UserRegister, UserAdress } from 'src/app/models/home.models';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public userRegisterForm :FormGroup = new FormGroup ({});

  public userAdressForm :FormGroup = new FormGroup ({});

  public submitted: boolean = false;

  constructor(private  formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.userRegisterForm = this.formBuilder.group(
      {
      name:['', [Validators.required, Validators.maxLength(20)]],
      password:['',[Validators.required, Validators.maxLength(20)]],
      passwordRepeat: ['', [Validators.required, Validators.maxLength(20)]],
      }
    );

    this.userAdressForm = this.formBuilder.group(
      {
        street : ['', [Validators.required]],
        number : ['', [Validators.required]],
        door : ['', [Validators.required]],
        postalCode: ['', [Validators.required]],
        city: ['', [Validators.required]],
        country: ['', [Validators.required]],
      }

    )


  }
  onSubmit(): void{
    this.submitted = true;
    console.log(this.userRegisterForm);

    if (this.userRegisterForm.valid) {

      setTimeout(()=>{

        const user: UserRegister = {

          name: this.userRegisterForm.get('name')?.value,
          password : this.userRegisterForm.get('password')?.value,
          passwordRepeat: this.userRegisterForm.get('passwordRepeat')?.value,

                          }
          console.log(user);


           this.userRegisterForm.reset();
           this.submitted = false;
      })
    }

  }

}

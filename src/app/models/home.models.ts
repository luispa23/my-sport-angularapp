export interface UserRegister {
  name: string;
  password: string;
  passwordRepeat: string;

}
export interface UserAdress {
  street : string;
  number : number;
  door : string;
  postalCode: Number;
  city: string;
  country: string;
}
